package fr.minecraft.MyRolePlugin.Listeners;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import Commons.MinecraftListener;
import fr.minecraft.MyRolePlugin.Models.Role;

public class MainListener extends MinecraftListener
{
	// quand le joueur join on lui ajoute ses droits
	@EventHandler
	public void onPlayerJoinEvent(PlayerJoinEvent event)
	{
		ArrayList<Role> roles = Role.GetRolesOf(event.getPlayer().getUniqueId());
		if(!roles.isEmpty())
			Role.GiveRightsTo(event.getPlayer());
		else
			event.getPlayer().sendMessage(ChatColor.MAGIC+"***"+
					ChatColor.GOLD+"Si vous souhaitez jouer sur le serveur c'est facile! Vous devez juste rejoindre le discord:"+ChatColor.WHITE+" https://discord.gg/JFNf7QpbVt"+ChatColor.GOLD+", puis suivre les instructions!"+
					ChatColor.WHITE+ChatColor.MAGIC+"***");
	}


	@SuppressWarnings("deprecation")
	@EventHandler
	public void guestCantInteract(PlayerInteractEvent event)
	{
		if(!event.isCancelled() && !event.getPlayer().isOp())
		{
			ArrayList<Role> roles = Role.GetRolesOf(event.getPlayer().getUniqueId());
			if(roles.isEmpty()) {
				event.setCancelled(true);
				event.getPlayer().sendMessage(ChatColor.RED+"Vous devez etre membre pour jouer sur ce serveur");
			}
		}
	}

	@EventHandler
	public void guestCantDamaged(EntityDamageByEntityEvent event) {
		if(!event.isCancelled())
		{
			Player player = null;

			//if plauer do direct damage
			if(event.getDamager() instanceof Player)
			{
				player = (Player) event.getDamager();
			}
			//if player do damage with projectile
			else if (event.getDamager() instanceof Projectile && (((Projectile)event.getDamager()).getShooter() instanceof Player))
			{
				player = (Player)((Projectile)event.getDamager()).getShooter();
			}

			if(player != null && Role.GetRolesOf(player.getUniqueId()).isEmpty() && !player.isOp())
			{
				event.setCancelled(true);
				player.sendMessage(ChatColor.RED+"Vous devez etre membre pour jouer sur ce serveur");
			}
		}
	}
}
