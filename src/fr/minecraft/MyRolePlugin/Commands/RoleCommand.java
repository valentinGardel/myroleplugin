package fr.minecraft.MyRolePlugin.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftCommand;
import fr.minecraft.MyRolePlugin.Models.Role;
import fr.minecraft.MyRolePlugin.TabCompleters.RoleTabCompleter;

public class RoleCommand extends MinecraftCommand
{
	static {
		COMMAND_NAME = "role";
		TAB_COMPLETER = RoleTabCompleter.class;
	}
	
	/**
	 * role [add | remove] [child | node] [role] [value]
	 * role create [name] [color] [power] [visible]
	 * role delete [role]
	 * role update [color | name | power | visible] [role] [value]
	 * */
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(args.length >=2 )
		{
			switch(args[0].toUpperCase())
			{
			case "ADD":
			case "REMOVE":
				switch(args[1].toUpperCase())
				{
				case "CHILD":
					if(args[0].equalsIgnoreCase("ADD"))
						this.addChild(sender, args);
					else
						this.removeChild(sender, args);
					break;
				case "NODE":
					if(args[0].equalsIgnoreCase("ADD"))
						this.addPermission(sender, args);
					else
						this.removePermission(sender, args);
					break;
				default:
					sender.sendMessage(ChatColor.RED+"Command invalide!");
					break;
				}
				break;
			case "DELETE":
				this.deleteRole(sender, args);
				break;
			case "CREATE":
				this.createRole(sender, args);
				break;
			case "UPDATE":
				this.updateRole(sender, args);
				break;
			default:
				sender.sendMessage(ChatColor.RED+"Command invalide!");
				break;
			}
		}
		else
			sender.sendMessage(ChatColor.RED+"Command invalide!");
		return true;
	}

	/** role [update] [color | name | power | visible] [role] [value] */
	private void updateRole(CommandSender sender, String[] args)
	{
		if(args.length == 4)
		{
			Role role = Role.GetRole(args[2]);
			if(role!=null)
			{
				switch(args[1].toUpperCase())
				{
				case "COLOR":
					this.updateColor(sender, role, args[3]);
					break;
				case "NAME":
					this.updateName(sender, role, args[3]);
					break;
				case "POWER":
					this.updatePower(sender, role, args[3]);
					break;
				case "VISIBLE":
					this.updateVisible(sender, role, args[3]);
					break;
				default:
					sender.sendMessage(ChatColor.RED+"Commande invalide!");
					break;
				}
			}
			else
				sender.sendMessage(ChatColor.RED+"Le role "+args[2]+" est introuvable!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide!");
	}

	/** role remove child [role] [child] */
	private void removeChild(CommandSender sender, String[] args)
	{
		if(args.length == 4)
		{
			Role role = Role.GetRole(args[2]);
			if(role!=null)
			{
				Role child = Role.GetRole(args[3]);
				if(child != null)
				{
					switch(role.removeChildren(child))
					{
					case Success:
						sender.sendMessage(ChatColor.GREEN+"Le role "+args[3]+" n'est plus un child de "+args[2]+"!");
						break;
					case ChildNotFound:
						sender.sendMessage(ChatColor.RED+"Le role "+args[3]+" n'est pas un child de "+args[2]+"!");
						break;
					default:
						sender.sendMessage(ChatColor.RED+"Faided!");
						break;
					}
				}
				else
					sender.sendMessage(ChatColor.RED+"Le role "+args[3]+" n'existe pas!");
			}
			else
				sender.sendMessage(ChatColor.RED+"Le role "+args[2]+" n'existe pas!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide!");
	}

	/** role add child [role] [child] */
	private void addChild(CommandSender sender, String[] args)
	{
		if(args.length == 4)
		{
			Role role = Role.GetRole(args[2]);
			if(role!=null)
			{
				Role child = Role.GetRole(args[3]);
				if(child != null)
				{
					switch(role.addChildren(child))
					{
					case Success:
						sender.sendMessage(ChatColor.GREEN+"Le role "+args[3]+" a �t� ajout� en child � "+args[2]+"!");
						break;
					case ChildAlreadyExist:
						sender.sendMessage(ChatColor.RED+"Le role "+args[3]+" est d�j� un child de "+args[2]+"!");
						break;
					case ChildToHigh:
						sender.sendMessage(ChatColor.RED+"Le role "+args[3]+" a une puissance trop grande pour �tre ajout� � "+args[2]+"!");
						break;
					default:
						sender.sendMessage(ChatColor.RED+"Faided!");
						break;
					}
				}
				else
					sender.sendMessage(ChatColor.RED+"Le role "+args[3]+" n'existe pas!");
			}
			else
				sender.sendMessage(ChatColor.RED+"Le role "+args[2]+" n'existe pas!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide!");
	}

	private void updateVisible(CommandSender sender, Role role, String value)
	{
		boolean visible = Boolean.parseBoolean(value);
		role.visible = visible;

		switch(role.save())
		{
		case Success:
			sender.sendMessage(ChatColor.GREEN+"Le role "+role.name+" est maintenant "+(visible?"visible!":"invisible!"));
			break;
		default:
			sender.sendMessage(ChatColor.RED+"Failed");
			break;
		}
	}

	private void updatePower(CommandSender sender, Role role, String value)
	{
		try {
			int power = Integer.parseInt(value);
			role.power = power;

			switch(role.save())
			{
			case Success:
				sender.sendMessage(ChatColor.GREEN+"Le role "+role.name+" a prit la puissance "+value+"!");
				break;
			default:
				sender.sendMessage(ChatColor.RED+"Failed!");
				break;
			}
		} catch(NumberFormatException e) {
			sender.sendMessage(ChatColor.RED+"La puissance est invalide!");
		}
	}

	private void updateName(CommandSender sender, Role role, String value)
	{
		if(Role.GetRole(value) == null)
		{
			role.name = value;
			switch(role.save())
			{
			case Success:
				sender.sendMessage(ChatColor.GREEN+"Le role a �t� renomm� "+value+"!");
				break;
			default:
				sender.sendMessage(ChatColor.RED+"Failed!");
				break;
			}
		}
		else
			sender.sendMessage(ChatColor.RED+"Le role "+value+" existe d�j�!");
	}

	private void updateColor(CommandSender sender, Role role, String value)
	{
		try {
			ChatColor color = ChatColor.valueOf(value);
			if(color!=null)
			{
				role.color = color;
				switch(role.save())
				{
				case Success:
					sender.sendMessage(ChatColor.GREEN+"Le role "+role.name+" a prit la couleur "+value+"!");
					break;
				default:
					sender.sendMessage(ChatColor.RED+"Failed!");
					break;
				}
			}
			else
				sender.sendMessage(ChatColor.RED+"Couleur invalide!");
		} catch (Exception e) {
			sender.sendMessage(ChatColor.RED+"Commande est invalide!");
		}
	}

	/** role remove node [role] [node] */
	private void removePermission(CommandSender sender, String[] args)
	{
		if(args.length == 4)
		{
			Role role = Role.GetRole(args[2]);
			if(role!=null)
			{
				switch(role.removePermission(args[3]))
				{
				case Success:
					sender.sendMessage(ChatColor.GREEN+"Le permission "+args[3]+" a �t� retir� du role "+args[2]+"!");
					break;
				case RoleNotFound:

					break;
				case PermissionNotFound:
					sender.sendMessage(ChatColor.RED+"Le role "+args[2]+" n'a pas la permission "+args[3]+"!");
					break;
				default:
					sender.sendMessage(ChatColor.RED+"Erreur lors de la supression de la node!");
					break;
				}
			}
			else
				sender.sendMessage(ChatColor.RED+"Le role "+args[2]+" n'existe pas!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide!");
	}

	/** role add node [role] [node] */
	private void addPermission(CommandSender sender, String[] args)
	{
		if(args.length == 4)
		{
			Role role = Role.GetRole(args[2]);
			if(role!=null)
			{
				switch(role.addPermission(args[3]))
				{
				case Success:
					sender.sendMessage(ChatColor.GREEN+"Le permission "+args[3]+" a �t� ajout� au role "+args[2]+"!");
					break;
				case PermissionAlreadyExist:
					sender.sendMessage(ChatColor.RED+"Le role "+args[2]+" a d�j� la permission "+args[3]+"!");
					break;
				default:
					sender.sendMessage(ChatColor.RED+"Erreur lors de l'ajout de permission!");
					break;
				}
			}
			else
				sender.sendMessage(ChatColor.RED+"Le role "+args[2]+" n'existe pas!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide!");
	}

	/** role delete [role] */
	private void deleteRole(CommandSender sender, String[] args)
	{
		if(args.length == 2)
		{
			Role role = Role.GetRole(args[1]);
			if(role!=null)
			{
				switch(role.delete())
				{
				case Success:
					sender.sendMessage(ChatColor.GREEN+"Le role "+args[1]+" a �t� supprim�!");
					break;
				default:
					sender.sendMessage(ChatColor.RED+"Erreur lors de la suppression!");
					break;
				}
			}
			else
				sender.sendMessage(ChatColor.RED+"Le role "+args[1]+" n'existe pas!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide!");
	}

	/** role create [name] [color] [power] [visible] */
	private void createRole(CommandSender sender, String[] args)
	{
		if(args.length == 5)
		{
			try{
				String roleName = args[1];
				ChatColor color = ChatColor.valueOf(args[2]);
				int power = Integer.parseInt(args[3]);
				boolean visible = Boolean.parseBoolean(args[4]);

				if(color != null)
				{
					Role role = new Role(color, roleName, power, visible);

					switch(role.save())
					{
					case Success:
						sender.sendMessage(ChatColor.GREEN+"Le role "+roleName+" a �t� cr��!");
						break;
					case RoleAlreadyExist:
						sender.sendMessage(ChatColor.RED+"Le role "+roleName+" existe d�j�!");
						break;
					default:
						sender.sendMessage(ChatColor.RED+"Erreur lors de la cr�ation du role!");
						break;
					}
				}
				else
					sender.sendMessage(ChatColor.RED+"La couleur est invalide!");
			} catch (Exception e) {
				sender.sendMessage(ChatColor.RED+"Commande est invalide!");
			}
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide!");
	}
}
