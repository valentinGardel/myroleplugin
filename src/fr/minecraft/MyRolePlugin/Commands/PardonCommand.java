package fr.minecraft.MyRolePlugin.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.BanEntry;
import org.bukkit.BanList.Type;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftCommand;
import fr.minecraft.MyRolePlugin.Models.Role;
import fr.minecraft.MyRolePlugin.TabCompleters.PardonTabCompleter;

public class PardonCommand extends MinecraftCommand
{
	static {
		COMMAND_NAME = "pardon";
		TAB_COMPLETER = PardonTabCompleter.class;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(args.length==1)
		{
			if(Bukkit.getBanList(Type.NAME).isBanned(args[0]))
			{
				BanEntry banEntry = Bukkit.getBanList(Type.NAME).getBanEntry(args[0]);
				if(sender.isOp())
				{
					Bukkit.getBanList(Type.NAME).pardon(args[0]);
					Bukkit.broadcast(ChatColor.GRAY+"Le joueur "+args[0]+" a �t� unban!", "MyRolePlugin.ban");
				}
				else if(sender instanceof Player)
				{
					Player player = (Player) sender;
					Role rolePlayer = Role.GetHigherRole(Role.GetRolesOf(player.getUniqueId()));
					@SuppressWarnings("deprecation")
					OfflinePlayer source = Bukkit.getOfflinePlayer(banEntry.getSource());

					if(source != null)
					{
						Role roleKickPlayer = Role.GetHigherRole(Role.GetRolesOf(source.getUniqueId()));

						if(roleKickPlayer==null || (rolePlayer!=null && rolePlayer.power>=roleKickPlayer.power))
						{
							Bukkit.getBanList(Type.NAME).pardon(args[0]);
							Bukkit.broadcast(ChatColor.GRAY+"Le joueur "+args[0]+" a �t� unban!", "MyRolePlugin.ban");
						}
						else
							sender.sendMessage(ChatColor.RED+"Vous ne pouvez pas unban quelqu'un bannie par quelqu'un plus haut dans la hi�rarchie que vous!");
					}
					else
						sender.sendMessage(ChatColor.RED+"Vous ne pouvez pas unban quelqu'un bannie par quelqu'un plus haut dans la hi�rarchie que vous!");
				}
				else
					sender.sendMessage(ChatColor.RED+"Vous n'avez pas la permission d'effectuer cette commande");
			}
			else
				sender.sendMessage(ChatColor.RED+"Le joueur "+args[0]+" n'est pas bannie!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide");
		return true;
	}
}
