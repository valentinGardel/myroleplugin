package fr.minecraft.MyRolePlugin.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftCommand;
import fr.minecraft.MyRolePlugin.Models.Role;

/**
 * Overload basic minecraft kick command
 * */
public class KickCommand extends MinecraftCommand
{
	static {
		COMMAND_NAME = "kick";
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(args.length>=1)
		{
			Player kickPlayer = Bukkit.getPlayer(args[0]);
			if(kickPlayer!=null && kickPlayer.isOnline())
			{
				if(sender.isOp())
				{
					this.kick(args, kickPlayer);
				}
				else
				{
					if(sender instanceof Player)
					{
						Player player = (Player) sender;
						Role rolePlayer = Role.GetHigherRole(Role.GetRolesOf(player.getUniqueId()));
						Role roleKickPlayer = Role.GetHigherRole(Role.GetRolesOf(kickPlayer.getUniqueId()));
						if(roleKickPlayer==null || (rolePlayer!=null && rolePlayer.power>roleKickPlayer.power))
							this.kick(args, kickPlayer);
						else
							sender.sendMessage(ChatColor.RED+"Vous ne pouvez pas kick un joueur plus haut dans la hiérarchie que vous!");
					}
					else
						sender.sendMessage(ChatColor.RED+"Vous n'avez pas la permission d'effectuer cette commande");
				}
			}
			else
				sender.sendMessage(ChatColor.RED+"Le joueur "+args[0]+" est introuvable!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide");
		return true;
	}

	/**
	 * Methode called to kick a player
	 * @param args arguments to potentialy get a reason (not null)
	 * @param kickPlayer player to kick
	 * */
	private void kick(String[] args,Player kickPlayer)
	{
		String reason = "";
		if(args.length>1)
		{
			for(int i=1;i<args.length;i++)
				reason+= args[i]+" ";
		}
		kickPlayer.kickPlayer(reason);
	}
}
