package fr.minecraft.MyRolePlugin.Commands;

import java.util.Date;

import org.bukkit.BanList.Type;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftCommand;
import fr.minecraft.MyRolePlugin.Models.Role;
import fr.minecraft.MyRolePlugin.TabCompleters.BanTabCompleter;

public class BanCommand extends MinecraftCommand
{
	static {
		COMMAND_NAME = "ban";
		TAB_COMPLETER = BanTabCompleter.class;
	}
	
	public static final String CONSOLE_NAME="console";
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(args.length>=1)
		{
			@SuppressWarnings("deprecation")
			OfflinePlayer banPlayer = Bukkit.getOfflinePlayer(args[0]);
			
			if(banPlayer!=null)
			{
				if(sender instanceof Player)
				{
					Player player = (Player) sender;
					Role rolePlayer = Role.GetHigherRole(Role.GetRolesOf(player.getUniqueId()));
					Role roleKickPlayer = Role.GetHigherRole(Role.GetRolesOf(banPlayer.getUniqueId()));
					if(player.isOp() || roleKickPlayer==null || (rolePlayer!=null && rolePlayer.power>roleKickPlayer.power))
					{
						this.ban(args, banPlayer, player.getName(), sender);
					}
					else
						sender.sendMessage(ChatColor.RED+"Vous ne pouvez pas ban un joueur plus haut dans la hi�rarchie que vous!");
				}
				else if(sender.isOp())
					this.ban(args, banPlayer, CONSOLE_NAME, sender);
				else
					sender.sendMessage(ChatColor.RED+"Vous n'avez pas la permission d'effectuer cette commande");
			}
			else
				sender.sendMessage(ChatColor.RED+"Le joueur "+args[0]+" est introuvable!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide");
		return true;
	}

	/**
	 * Methode called to ban a player
	 * @param args arguments to potentialy get a reason (not null)
	 * @param banPlayer player to ban
	 * @param source of the command
	 * */
	private void ban(String[] args, OfflinePlayer banPlayer, String source, CommandSender sender)
	{
		Date date=null;
		try {
			date=this.getDate(args);
		} catch(Exception e) {
			sender.sendMessage(ChatColor.RED+"Le temps de ban est invalide.");
			return;
		}
		String reason = "";
		if(args.length>2)
		{
			for(int i=2;i<args.length;i++)
				reason+= args[i]+" ";
		}
		else
			reason="etre un gros naze";
		Bukkit.getBanList(Type.NAME).addBan(banPlayer.getName(), reason, date, source);
		if(banPlayer.isOnline())
			banPlayer.getPlayer().kickPlayer("banned");
		Bukkit.broadcastMessage(ChatColor.GRAY+"Le joueur "+banPlayer.getName()+" a �t� banni pour: "+reason);
	}

	private Date getDate(String[] args)
	{
		if(args.length>=2 && !args[1].equalsIgnoreCase("def"))
		{
			int minutes = Integer.parseInt(args[1]) * 60000;
			return new Date(new Date().getTime() + minutes );
		}
		else
			return null;
	}
}
