package fr.minecraft.MyRolePlugin.Commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftCommand;
import fr.minecraft.MyRolePlugin.Models.Role;
import fr.minecraft.MyRolePlugin.TabCompleters.PermissionTabCompleter;

public class PermissionCommand extends MinecraftCommand
{
	static {
		COMMAND_NAME = "permission";
		TAB_COMPLETER = PermissionTabCompleter.class;
	}

	/**
	 * permission [info | add | remove] [player] [<node>]
	 * */
	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(args.length==2 && args[0].equalsIgnoreCase("info"))
		{
			OfflinePlayer player = Bukkit.getOfflinePlayer(args[1]);
			if(player!=null)
			{
				ArrayList<String> nodes = Role.GetPlayerPermissions(player);
				sender.sendMessage(ChatColor.GOLD+"Permissions du joueur:");
				for(String node : nodes)
					sender.sendMessage(" - "+node);
				if(nodes.isEmpty())
					sender.sendMessage("aucune permission assign�");
			}
			else sender.sendMessage(ChatColor.RED+"Le joueur "+args[1]+" est introuvable!");
		}
		else if(args.length==3)
		{
			if(args[0].equalsIgnoreCase("add"))
			{
				OfflinePlayer player = Bukkit.getOfflinePlayer(args[1]);
				if(player!=null)
				{
					switch(Role.AddPlayerPermission(player, args[2]))
					{
					case Success:
						sender.sendMessage(ChatColor.GREEN+"Permission "+args[2]+" ajout� au joueur "+args[1]+"!");
						break;
					default:
						sender.sendMessage(ChatColor.RED+"Failed");
						break;
					}
				}
				else sender.sendMessage(ChatColor.RED+"Le joueur "+args[1]+" est introuvable!");
			}
			else if(args[0].equalsIgnoreCase("remove"))
			{
				OfflinePlayer player = Bukkit.getOfflinePlayer(args[1]);
				if(player!=null)
				{
					switch(Role.RemovePlayerPermissions(player, args[2]))
					{
					case Success:
						sender.sendMessage(ChatColor.GREEN+"Permission "+args[2]+" enlev� au joueur "+args[1]+"!");
						break;
					default:
						sender.sendMessage(ChatColor.RED+"Failed");
						break;
					}
				}
				else sender.sendMessage(ChatColor.RED+"Le joueur "+args[1]+" est introuvable!");
			}
			else
				sender.sendMessage(ChatColor.RED+"Commande invalide!");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide!");
		return true;
	}
}
