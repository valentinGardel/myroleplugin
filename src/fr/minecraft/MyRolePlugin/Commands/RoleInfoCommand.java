package fr.minecraft.MyRolePlugin.Commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftCommand;
import Commons.MessageBuilder;
import fr.minecraft.MyRolePlugin.Models.Role;
import fr.minecraft.MyRolePlugin.TabCompleters.RoleInfoTabCompleter;

public class RoleInfoCommand extends MinecraftCommand
{
	static {
		COMMAND_NAME = "roleInfo";
		TAB_COMPLETER = RoleInfoTabCompleter.class;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(label.equalsIgnoreCase("roleinfo"))
			this.commandRole(sender, args);
		else if(label.equalsIgnoreCase("roleOf"))
			this.commandRoleOf(sender, args);
		else
			sender.sendMessage(ChatColor.RED+"Vous n'avez pas la permission d'utiliser cette commande");

		return true;
	}

	/*
	private void commandPermInfo(CommandSender sender, String[] args)
	{
		if(args.length == 1)
		{
			Plugin plugin = Bukkit.getPluginManager().getPlugin(args[0]);

			if(plugin == null)
			{
				sender.sendMessage(ChatColor.RED+"Le plugin "+args[0]+" n'existe pas");
			}
			else
			{
				sender.sendMessage(ChatColor.GOLD+"Liste des permissions du plugin "+args[0]+":");
				for(Permission i : plugin.getDescription().getPermissions())
					sender.sendMessage(" "+i.getName()+": "+i.getDescription());
			}
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide");

	}
	*/

	private void commandRoleOf(CommandSender sender, String[] args) {
		if(args.length == 1) {
			@SuppressWarnings("deprecation")
			OfflinePlayer player = Bukkit.getOfflinePlayer(args[0]);

			if(player != null)
			{
				ArrayList<Role> roles = Role.GetRolesOf(player.getUniqueId());
				if(roles.isEmpty())
					sender.sendMessage(ChatColor.RED+"Le joueur "+args[0]+" n'a aucun role.");
				else
				{
					sender.sendMessage(ChatColor.GOLD+"Le joueur "+args[0]+" a "+roles.size()+" role(s): ");
					MessageBuilder message;
					for(Role role : roles)
					{
						message = MessageBuilder.create(role.color+" "+role.name);
						message.onHoverText("Info").onClickRunCommand("/roleinfo "+role.name).write(ChatColor.DARK_GREEN+" [Info]");
						message.onHoverText("Demote").onClickSugestCommand("/demote "+args[0]+" "+role.name).write(ChatColor.DARK_RED+" [Demote]");
						message.send(sender);
					}
				}
			}
			else
				sender.sendMessage(ChatColor.RED+"Le joueur "+args[0]+" n'existe pas");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide");

	}

	private void commandRole(CommandSender sender, String[] args)
	{
		//Si on donne la liste de tout les roles existant
		if(args.length == 0)
		{
			ArrayList<Role> roles = Role.GetRoles();

			if(roles.isEmpty())
				sender.sendMessage(ChatColor.RED+"Aucun role existant");
			else
			{
				sender.sendMessage(ChatColor.GOLD+""+roles.size()+" role(s) trouv� :");
				MessageBuilder message;
				for(Role role : roles)
				{
					message = MessageBuilder.create(" "+role.toString());
					message.onHoverText("Info").onClickRunCommand("/roleinfo "+role.name).write(ChatColor.DARK_GREEN+" [Info]");
					message.send(sender);
				}
			}
		}
		else if(args.length == 1)
		{
			Role role =  Role.GetRole(args[0]);

			if(role != null)
			{
				MessageBuilder message = MessageBuilder.create(" "+role.toString());
				message.onHoverText("Permissions").onClickRunCommand("/roleinfo "+role.name+" permissions").write(ChatColor.DARK_GREEN+" [Permissions]");
				message.onHoverText("Players").onClickRunCommand("/roleinfo "+role.name+" players").write(ChatColor.DARK_GREEN+" [Players]");
				message.onHoverText("Childs").onClickRunCommand("/roleinfo "+role.name+" childs").write(ChatColor.DARK_GREEN+" [Childs]");
				message.onHoverText("Supprimer").onClickSugestCommand("/role delete "+role.name).write(ChatColor.DARK_RED+" [Supprimer]");
				message.send(sender);
			}
			else
				sender.sendMessage(ChatColor.RED+"Le role "+args[0]+" n'existe pas");
		}
		else if(args.length == 2)
		{
			Role role =  Role.GetRole(args[0]);

			if(role != null)
			{
				MessageBuilder message;
				switch(args[1])
				{
				case "permissions":
					ArrayList<String> permissions = role.getPermissions();
					sender.sendMessage(ChatColor.GOLD+"Le role "+args[0]+" a "+permissions.size()+" permission(s):");
					
					for(String permission : permissions)
					{
						message = MessageBuilder.create(" "+permission);
						message.onHoverText("Remove").onClickSugestCommand("/role remove node "+role.name+" "+permission).write(ChatColor.DARK_RED+" [Remove]");
						message.send(sender);
					}
					break;
				case "players":
					ArrayList<OfflinePlayer> players = role.getPlayers();
					sender.sendMessage(ChatColor.GOLD+"Le role "+args[0]+" est attribu� a "+players.size()+" joueur(s):");
					for(OfflinePlayer player : players)
					{
						message = MessageBuilder.create(" "+player.getName());
						message.onHoverText("Demote").onClickSugestCommand("/demote "+player.getName()+" "+role.name).write(ChatColor.DARK_RED+" [Demote]");
						message.send(sender);
					}
					break;
				case "childs":
					ArrayList<Role> childs = role.getChilds();
					sender.sendMessage(ChatColor.GOLD+"Le role "+args[0]+" a "+childs.size()+" child(s):");
					for(Role child : childs)
					{
						message = MessageBuilder.create(" "+child.name);
						message.onHoverText("Remove").onClickSugestCommand("/role remove child "+role.name+" "+child.name).write(ChatColor.DARK_RED+" [Remove]");
						message.send(sender);
					}
					break;
				}
			}
			else
				sender.sendMessage(ChatColor.RED+"Le role "+args[0]+" n'existe pas");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide");

	}
}
