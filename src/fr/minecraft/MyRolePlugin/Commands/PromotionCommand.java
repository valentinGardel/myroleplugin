package fr.minecraft.MyRolePlugin.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftCommand;
import fr.minecraft.MyRolePlugin.Models.Role;
import fr.minecraft.MyRolePlugin.TabCompleters.PromotionTabCompleter;
import fr.minecraft.MyRolePlugin.Utils.CodeRole;

public class PromotionCommand extends MinecraftCommand
{
	static {
		COMMAND_NAME = "promote";
		TAB_COMPLETER = PromotionTabCompleter.class;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{

		if(label.equals("promote"))
			this.promote(sender, args);
		else if(label.equals("demote"))
			this.demote(sender, args);
		return true;
	}

	public void promote(CommandSender sender, String[] args) {
		//args[0] = player
		//args[1] = role
		if(args.length == 2)
		{
			Role role = Role.GetRole(args[1]);

			if(role != null)
			{
				@SuppressWarnings("deprecation")
				OfflinePlayer player = Bukkit.getOfflinePlayer(args[0]);

				if(player!=null)
				{
					CodeRole code = role.promote(sender, player);

					switch(code)
					{
					case Success:
						sender.sendMessage(ChatColor.GREEN+"Le role "+args[1]+" a �t� donn� � "+args[0]);
						break;
					case PlayerHasAlreadyRole:
						sender.sendMessage(ChatColor.RED+"Le joueur "+args[0]+" possede d�j� le role "+args[1]);
						break;
					case CantGiveThisRole:
						sender.sendMessage(ChatColor.RED+"Vous ne pouvez pas donner le role "+args[1]+". Vous ne pouvez pas promouvoir quelqu'un � un grade �gal ou sup�rieur � vous");
						break;
					default:
						sender.sendMessage(ChatColor.RED+"Erreur promote");
						break;
					}
				}
				else
					sender.sendMessage(ChatColor.RED+"Le joueur "+args[0]+" n 'existe pas");
			}
			else
				sender.sendMessage(ChatColor.RED+"Le role "+args[1]+" n'existe pas");
		}
		else {
			sender.sendMessage(ChatColor.RED+"Commande invalide");
		}
	}

	public void demote(CommandSender sender, String[] args) 
	{
		if(args.length==2)
		{
			Role role = Role.GetRole(args[1]);
			if(role != null)
			{
				@SuppressWarnings("deprecation")
				OfflinePlayer player = Bukkit.getOfflinePlayer(args[0]);

				if(player!=null)
				{
					CodeRole code = role.demote(sender, player);

					switch(code)
					{
					case Success:
						sender.sendMessage(ChatColor.GREEN+"Le role "+args[1]+" a �t� suprim� � "+args[0]);
						break;
					case PlayerDontHaveRole:
						sender.sendMessage(ChatColor.RED+"Le joueur "+args[0]+" ne possede pas le role "+args[1]);
						break;
					case CantGiveThisRole:
						sender.sendMessage(ChatColor.RED+"Vous ne pouvez pas enlever le role "+args[1]+". Vous ne pouvez pas demote quelqu'un poss�dant un grade �gal ou sup�rieur � vous");
						break;
					default:
						sender.sendMessage(ChatColor.RED+"Erreur demote");
						break;
					}
				}
				else
					sender.sendMessage(ChatColor.RED+"Le joueur "+args[0]+" n 'existe pas");
			}
			else
				sender.sendMessage(ChatColor.RED+"Le role "+args[1]+" n'existe pas");
		}
		else if(args.length==1)
		{
			CodeRole code = Role.Demote(sender, args[0]);

			switch(code)
			{
			case Success:
				sender.sendMessage(ChatColor.GREEN+"Les roles du joueur "+args[0]+" ont �t� supprim�");
				break;
			case PlayerDontHaveRole:
				sender.sendMessage(ChatColor.RED+"Le joueur "+args[0]+" ne possede aucun role ");
				break;
			case CantGiveThisRole:
				sender.sendMessage(ChatColor.RED+"Vous ne pouvez pas demote quelqu'un � un grade �gal ou sup�rieur � vous");
				break;
			default:
				sender.sendMessage(ChatColor.RED+"Erreur demote");
				break;
			}
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande invalide");
	}
}
