package fr.minecraft.MyRolePlugin;

import Commons.MinecraftPlugin;
import fr.minecraft.MyRolePlugin.Commands.*;
import fr.minecraft.MyRolePlugin.Listeners.*;
import fr.minecraft.MyRolePlugin.Models.*;

@SuppressWarnings("unchecked")
public class Main extends MinecraftPlugin
{
	static {
		PLUGIN_NAME = "MyRolePlugin";
		LOG_TOGGLE = false;
		COMMANDS = new Class[]{
				RoleCommand.class,
				RoleInfoCommand.class,
				PermissionCommand.class,
				PromotionCommand.class,
				KickCommand.class,
				BanCommand.class,
				PardonCommand.class
			};
		LISTENERS = new Class[]{
				MainListener.class
			};
		CONFIG = Config.class;
	}
	
	@Override
	protected void initDatabase() throws Exception
	{
		Role.CreateTable();
	}
	
}
