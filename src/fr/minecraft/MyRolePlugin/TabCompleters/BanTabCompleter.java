package fr.minecraft.MyRolePlugin.TabCompleters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftTabCompleter;

public class BanTabCompleter extends MinecraftTabCompleter
{

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args)
	{
		List<String> res = new ArrayList<String>();
		switch(args.length)
		{
		case 1:
			res=null;
			break;
		case 2:
			res.add("def");
			res.add("[<minutes>]");
			break;
		case 3:
			res.add("<reason>");
			break;
		default:
			res=null;
			break;
		}
		return res;
	}

}
