package fr.minecraft.MyRolePlugin.TabCompleters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.permissions.Permission;

import Commands.MinecraftTabCompleter;
import fr.minecraft.MyRolePlugin.Models.Role;

public class RoleTabCompleter extends MinecraftTabCompleter
{
	/**
	 * role [add | remove] [child | node] [role] [value]
	 * role create [name] [color] [power] [visible]
	 * role delete [role]
	 * role update [color | name | power | visible] [role] [value]
	 * */
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args)
	{
		List<String> res=null;

		switch(args.length)
		{
		case 1:
			res = new ArrayList<String>();
			res.add("ADD");
			res.add("REMOVE");
			res.add("CREATE");
			res.add("DELETE");
			res.add("UPDATE");
			break;
		case 2:
			res = this.onArg2(args);
			break;
		case 3:
			res = this.onArg3(args);
			break;
		case 4:
			res = this.onArg4(args);
			break;
		case 5:
			res = this.onArg5(args);
			break;
		}
		return res;
	}

	private List<String> onArg5(String[] args)
	{
		List<String> res=new ArrayList<String>();

		switch(args[0])
		{
		case "CREATE":
			if(args[4].isEmpty())
				res.add("[visible]");
			else
			{
				res.add("TRUE");
				res.add("FALSE");
			}
			break;
		}

		return res;
	}

	private List<String> onArg4(String[] args)
	{
		List<String> res=new ArrayList<String>();

		switch(args[0])
		{
		case "ADD":
			switch(args[1])
			{
			case "NODE":
				Role role = Role.GetRole(args[2]);
				if(role != null)
				{
					ArrayList<String> rolePerm = role.getPermissions();
					for(Permission permission : Bukkit.getPluginManager().getPermissions())
						if(permission.getName().toUpperCase().startsWith(args[3].toUpperCase()) && !rolePerm.contains(permission.getName()))
							res.add(permission.getName());
				}
				break;
			case "CHILD":
				res = this.getRoleStartWith(args[3]);
				Role role3 = Role.GetRole(args[2]);
				if(role3 != null)
				{
					for(Role child : role3.getChilds())
						res.remove(child.name);
					res.remove(role3.name);
				}
				break;
			}
			break;
		case "REMOVE":
			switch(args[1])
			{
			case "NODE":
				Role role2 = Role.GetRole(args[2]);
				if(role2 != null)
				{
					for(String permission : role2.getPermissions())
					{
						if(permission.toUpperCase().startsWith(args[3].toUpperCase()))
							res.add(permission);
					}
				}
				break;
			case "CHILD":
				Role role3 = Role.GetRole(args[2]);
				if(role3 != null)
				{
					for(Role child : role3.getChilds())
						res.add(child.name);
				}
				break;
			}
			break;
		case "CREATE":
			if(args[3].isEmpty())
				res.add("[power]");
			break;
		case "UPDATE":
			if(args[3].isEmpty())
				res.add("[value]");
			else
			{
				switch(args[1])
				{
				case "COLOR":
					res = this.getColorStartWith(args[3]);
					break;
				case "VISIBLE":
					res.add("TRUE");
					res.add("FALSE");
					break;
				}
			}
			break;
		}

		return res;
	}

	private List<String> onArg3(String[] args)
	{
		List<String> res=new ArrayList<String>();

		switch(args[0])
		{
		case "ADD":
		case "REMOVE":
		case "UPDATE":
			res = this.getRoleStartWith(args[2]);
			break;
		case "CREATE":
			if(args[2].isEmpty())
				res.add("[color]");
			else
			{
				res = this.getColorStartWith(args[2]);
			}
			break;
		}

		return res;
	}

	private List<String> onArg2(String[] args)
	{
		List<String> res=new ArrayList<String>();

		switch(args[0])
		{
		case "ADD":
		case "REMOVE":
			res.add("CHILD");
			res.add("NODE");
			break;
		case "CREATE":
			if(args[1].isEmpty())
				res.add("[name]");
			break;
		case "DELETE":
			res = this.getRoleStartWith(args[1]);
			break;
		case "UPDATE":
			res.add("COLOR");
			res.add("NAME");
			res.add("POWER");
			res.add("VISIBLE");
			break;
		default:
			res=null;
			break;
		}
		return res;
	}

	private List<String> getColorStartWith(String value)
	{
		List<String> res=new ArrayList<String>();
		for(ChatColor color: ChatColor.values())
		{
			if(color.name().toUpperCase().startsWith(value.toUpperCase()))
				res.add(color.name());
		}
		return res;
	}

	private List<String> getRoleStartWith(String start)
	{
		List<String> res=new ArrayList<String>();
		for(Role role: Role.GetRoles())
		{
			if(role.name.toUpperCase().startsWith(start.toUpperCase()))
				res.add(role.name);
		}
		return res;
	}
}