package fr.minecraft.MyRolePlugin.TabCompleters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftTabCompleter;
import fr.minecraft.MyRolePlugin.Models.Role;

public class RoleInfoTabCompleter extends MinecraftTabCompleter
{

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args)
	{
		List<String> res=null;

		if(sender instanceof Player)
		{
			switch(label)
			{
			case "roleinfo":
				res = this.forRole(sender, args);
				break;
			}
		}
		return res;
	}

	private List<String> forRole(CommandSender sender, String[] args) {
		List<String> res= new ArrayList<String>();

		switch(args.length)
		{
		case 1:
			ArrayList<Role> roles = Role.GetRoles();
			for(Role role: roles)
			{
				if(role.name.startsWith(args[0]))
					res.add(role.name);
			}
			break;
		case 2:
			res.add("players");
			res.add("permissions");
			res.add("childs");
			break;
		}

		return res;
	}

}
