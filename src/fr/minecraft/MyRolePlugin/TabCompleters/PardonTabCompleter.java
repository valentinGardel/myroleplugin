package fr.minecraft.MyRolePlugin.TabCompleters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.BanEntry;
import org.bukkit.BanList.Type;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import Commands.MinecraftTabCompleter;

public class PardonTabCompleter extends MinecraftTabCompleter
{

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args)
	{
		List<String> res = null;
		if(args.length==1)
		{
			res = new ArrayList<String>();
			for(BanEntry entry : Bukkit.getBanList(Type.NAME).getBanEntries())
				res.add(entry.getTarget());
		}
		return res;
	}

}
