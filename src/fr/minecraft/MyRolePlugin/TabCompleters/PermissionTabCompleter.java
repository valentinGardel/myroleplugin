package fr.minecraft.MyRolePlugin.TabCompleters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.permissions.Permission;

import Commands.MinecraftTabCompleter;
import fr.minecraft.MyRolePlugin.Models.Role;

public class PermissionTabCompleter extends MinecraftTabCompleter
{

	@SuppressWarnings("deprecation")
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args)
	{
		ArrayList<String> res = new ArrayList<String>();
		switch(args.length)
		{
		case 1:
			res.add("info");
			res.add("add");
			res.add("remove");
			break;
		case 2:
			res=null;
			break;
		case 3:
			if(args[0].equalsIgnoreCase("remove"))
			{
				OfflinePlayer player = Bukkit.getOfflinePlayer(args[1]);
				if(player != null)
					res = Role.GetPlayerPermissions(player);
			}
			else 
			{
				for(Permission permission : Bukkit.getPluginManager().getPermissions())
					if(permission.getName().toUpperCase().startsWith(args[2].toUpperCase()))
						res.add(permission.getName());
			}
			break;
		}
		return res;
	}
}
