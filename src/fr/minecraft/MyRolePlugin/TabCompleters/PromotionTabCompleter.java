package fr.minecraft.MyRolePlugin.TabCompleters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftTabCompleter;
import fr.minecraft.MyRolePlugin.Models.Role;

public class PromotionTabCompleter extends MinecraftTabCompleter
{

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args)
	{
		List<String> res=null;

		if(sender instanceof Player)
		{
			//Player player = (Player) sender;
			switch(label)
			{
			case "promote":
				res = this.forPromote(args);
				break;
			case "demote":
				res = this.forDemote(args);
				break;
			}
		}
		return res;
	}

	private List<String> forPromote(String[] args)
	{
		List<String> res= new ArrayList<String>();

		switch(args.length)
		{
		case 1:
			res=null;
			break;
		case 2:
			ArrayList<Role> roles = Role.GetRoles();
			for(Role role: roles)
			{
				if(role.name.startsWith(args[1]))
					res.add(role.name);
			}
			break;
		}

		return res;
	}

	private List<String> forDemote(String[] args)
	{
		List<String> res= new ArrayList<String>();

		switch(args.length)
		{
		case 2:
			Player player = Bukkit.getPlayer(args[0]);
			if(player != null)
			{
				ArrayList<Role> roles = Role.GetRolesOf(player.getUniqueId());
				for(Role role: roles)
				{
					if(role.name.startsWith(args[1]))
						res.add(role.name);
				}
			}
			else
				res = null;
			break;
		case 1:
			res=null;
			break;
		}

		return res;
	}

}
