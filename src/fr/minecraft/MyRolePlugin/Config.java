package fr.minecraft.MyRolePlugin;

import org.bukkit.plugin.java.JavaPlugin;
import Commons.DatabaseConfig;

public class Config extends DatabaseConfig
{
	private static Config instance;
	
	public void initConfig(JavaPlugin plugin)
	{
		super.initConfig(plugin);
		Config.instance = this;
	}
	
	public static Config GetConfig()
	{
		return Config.instance;
	}
}
