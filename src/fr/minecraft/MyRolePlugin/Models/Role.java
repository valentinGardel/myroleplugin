package fr.minecraft.MyRolePlugin.Models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.plugin.Plugin;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;

import ActiveRecord.Model;
import fr.minecraft.MyRolePlugin.Config;
import fr.minecraft.MyRolePlugin.Utils.CodeRole;

public class Role extends Model
{
	public static final String TABLE_ROLE="Role", TABLE_ROLE_CHILD="RoleChild", TABLE_ROLE_NODE="RoleNode", TABLE_PLAYER_ROLE="PlayerRole", TABLE_PLAYER_PERMISSION="PlayerPermission";

	private int id=-1;

	public int power;
	public ChatColor color;
	public String name;
	public boolean visible, oldVisible;

	private String oldName;


	/**
	 * Construit un Role avec un nom et une couleur
	 * @param color couleur du role
	 * @param name nom du role
	 * */
	public Role(ChatColor color, String name, int power, boolean visible)
	{
		this.color = color;
		this.name = name;
		this.power = power;
		this.visible = visible;
		this.oldVisible = visible;
	}

	/**
	 * Cr�e les tables n�cessaire au plugin si elle n'existe pas 
	 * @throws SQLException 
	 * */
	public static void CreateTable() throws SQLException
	{
		//table role
		//table rolePerm
		//table roleChild
		//table playerRole
		Statement stmt = Config.GetConfig().getConnection().createStatement();
		stmt.executeUpdate("create table if not exists "+TABLE_ROLE+"(id INT PRIMARY KEY AUTO_INCREMENT, name varchar(50) unique, color varchar(50), power int, visible bool);");
		stmt.executeUpdate("create table if not exists "+TABLE_PLAYER_ROLE+"(roleId INT, player varchar(50), foreign key(roleId) references "+TABLE_ROLE+"(id) ON DELETE CASCADE, primary key(roleId, player));");
		stmt.executeUpdate("create table if not exists "+TABLE_ROLE_NODE+"(roleId int, node varchar(50), foreign key(roleId) references "+TABLE_ROLE+"(id) ON DELETE CASCADE, primary key(roleId, node));");
		stmt.executeUpdate("create table if not exists "+TABLE_ROLE_CHILD+"(roleId int, roleChild int,foreign key (roleId) references "+TABLE_ROLE+"(id) ON DELETE CASCADE, foreign key (roleChild) references "+TABLE_ROLE+"(id) ON DELETE CASCADE, primary key (roleId, roleChild));");
		stmt.executeUpdate("create table if not exists "+TABLE_PLAYER_PERMISSION+"(player varchar(50), node varchar(50), PRIMARY KEY(player, node));");
		stmt.close();
	}

	/**
	 * Get un role par son nom
	 * @param name nom du role
	 * @return role correspondant au nom, null si notFound
	 * */
	public static Role GetRole(String name)
	{
		Role role=null;

		try {
			Connection con = Config.GetConfig().getConnection();
			PreparedStatement st=con.prepareStatement("Select id, name, power, visible, color from "+TABLE_ROLE+" where name=?;");
			st.setString(1, name);
			ResultSet res = st.executeQuery();
			if(res.next())
			{
				role=new Role(
						ChatColor.valueOf(res.getString("color")),
						res.getString("name"),
						res.getInt("power"),
						res.getBoolean("visible")
						);
				role.id=res.getInt("id");
			}
		} catch (SQLException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getLocalizedMessage());
		}

		return role;
	}

	/**
	 * Enregistre un role dans la bdd
	 * @param role a ajouter
	 * @return code de statut
	 * */
	public CodeRole save()
	{
		//if update
		if(this.id>0)
		{
			if(this.name != this.oldName && Role.GetRole(this.name)!=null)
			{
				return CodeRole.RoleAlreadyExist;
			}
			else
			{
				//update role
				try {
					PreparedStatement st = Config.GetConfig().getConnection().prepareStatement("UPDATE "+TABLE_ROLE+" SET name=?, power=?, color=?, visible=? where id=?");
					st.setString(1, this.name);
					st.setInt(2, this.power);
					st.setString(3, this.color.name());
					st.setBoolean(4, this.visible);
					st.setInt(5, this.id);
					st.executeUpdate();

					if(this.visible)
					{
						ScoreboardManager sbm = Bukkit.getScoreboardManager();
						//if name changed
						if(this.name != this.oldName)
						{
							Team oldTeam = sbm.getMainScoreboard().getTeam(this.oldName);
							//if team already exists
							if(oldTeam!=null)
							{
								oldTeam.setDisplayName(this.name);
								oldTeam.setColor(this.color);
							}
							//if team not exists
							else
							{
								Team team = sbm.getMainScoreboard().getTeam(this.name);
								if(team==null)
								{
									sbm.getMainScoreboard().registerNewTeam(this.name).setColor(this.color);
									ArrayList<OfflinePlayer> players = this.getPlayers();
									for(OfflinePlayer player : players)
										Role.UpdateTeam(player, Role.GetRolesOf(player.getUniqueId()));
								}
								else
								{
									Bukkit.getConsoleSender().sendMessage(ChatColor.RED+"La team "+this.name+" existe deja, cette team serat utilis� pour les roles!");
									team.setColor(this.color);
								}
							}
						}
						//if name is the same
						else
						{
							Team team = sbm.getMainScoreboard().getTeam(this.name);
							if(team==null)
							{
								sbm.getMainScoreboard().registerNewTeam(this.name).setColor(this.color);
								ArrayList<OfflinePlayer> players = this.getPlayers();
								for(OfflinePlayer player : players)
									Role.UpdateTeam(player, Role.GetRolesOf(player.getUniqueId()));
							}
							else
							{
								team.setColor(this.color);
								Bukkit.getConsoleSender().sendMessage(ChatColor.RED+"La team "+this.name+" existe deja, cette team serat utilis� pour les roles!");
							}
						}

					}
					else if(this.oldVisible && !this.visible)
					{
						ScoreboardManager sbm = Bukkit.getScoreboardManager();
						Team team = sbm.getMainScoreboard().getTeam(this.oldName);
						if(team!=null)
							team.unregister();
					}

				} catch (SQLException e) {
					Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getLocalizedMessage());
					return CodeRole.Failed;
				}
			}
		}
		//if create
		else
		{
			//if name doesnt exists
			if(Role.GetRole(this.name)==null)
			{
				//create role
				try {
					PreparedStatement st = Config.GetConfig().getConnection().prepareStatement("insert into "+TABLE_ROLE+" (name, power, color, visible) values(?, ?, ?, ?);");
					st.setString(1, this.name);
					st.setInt(2, this.power);
					st.setString(3, this.color.name());
					st.setBoolean(4, this.visible);
					st.executeUpdate();

					if(this.visible)
					{
						ScoreboardManager sbm = Bukkit.getScoreboardManager();
						Team team = sbm.getMainScoreboard().getTeam(this.name);
						if(team==null)
						{
							Bukkit.getConsoleSender().sendMessage(ChatColor.RED+"team don't exists => creation");
							team = sbm.getMainScoreboard().registerNewTeam(this.name);
							team.setColor(this.color);
							Bukkit.getConsoleSender().sendMessage(ChatColor.RED+"team created");
						}
					}
				} catch (SQLException e) {
					Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getLocalizedMessage());
					return null;
				}
			}
			else
				return CodeRole.RoleAlreadyExist;
		}
		return CodeRole.Success;
	}

	/**
	 * Update the team of the given player in function of the given roles
	 * @param player to update team
	 * @param roles where to check team (roles of player)
	 */
	private static void UpdateTeam(OfflinePlayer player, ArrayList<Role> roles)
	{
		if(player != null && roles!=null)
		{
			ArrayList<Role> visibleRoles = new ArrayList<Role>();

			for(Role role : roles)
			{
				if(role.visible)
					visibleRoles.add(role);
			}

			Role role = Role.GetHigherRole(visibleRoles);

			ScoreboardManager sbm = Bukkit.getScoreboardManager();

			if(role==null)
			{
				Team team = sbm.getMainScoreboard().getEntryTeam(player.getName());//.getPlayerTeam(player);
				if(team != null)
					team.removeEntry(player.getName());//.removePlayer(player);
			}
			else
			{
				Team team = sbm.getMainScoreboard().getTeam(role.name);
				if(team != null)
					team.addEntry(player.getName());//.addPlayer(player);
			}
		}
	}

	/**
	 * Supprime un role dans la bdd
	 * @param role nom du role a supprimer
	 * @return code de status
	 * */
	public CodeRole delete()
	{
		if(this.id>0)
		{
			try {
				ArrayList<OfflinePlayer> players = this.getPlayers();

				Connection con = Config.GetConfig().getConnection();
				PreparedStatement st;

				st = con.prepareStatement("delete from "+TABLE_ROLE+" where id=?;");
				st.setInt(1, this.id);
				st.executeUpdate();
				st.close();

				for(OfflinePlayer player: players)
					Role.UpdateTeam(player, Role.GetRolesOf(player.getUniqueId()));

				this.id=-1;

			} catch (SQLException e) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getLocalizedMessage());
				return null;
			}
		}
		else
			return CodeRole.RoleNotFound;
		return CodeRole.Success;
	}


	/**
	 * Get les role d'un joueur
	 * @param playerUUID uuid du joueur dont on renvoie les roles
	 * @return liste des roles
	 * */
	public static ArrayList<Role> GetRolesOf(UUID playerUUID)
	{
		ArrayList<Role> roles = new ArrayList<Role>();
		try {
			Connection con = Config.GetConfig().getConnection();
			PreparedStatement st=con.prepareStatement("Select id, name, power, visible, color from "+TABLE_ROLE+" inner join "+TABLE_PLAYER_ROLE+" on "+TABLE_ROLE+".id="+TABLE_PLAYER_ROLE+".roleId where player=?;");
			st.setString(1, playerUUID.toString());
			ResultSet res = st.executeQuery();

			Role role;
			while(res.next())
			{
				role=new Role(
						ChatColor.valueOf(res.getString("color")),
						res.getString("name"),
						res.getInt("power"),
						res.getBoolean("visible")
						);
				role.id=res.getInt("id");
				roles.add(role);
			}
			res.close();
			st.close();
		} catch (SQLException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getLocalizedMessage());
		}
		return roles;
	}


	/**
	 * Renvoie le role le plus haut dans le liste donn�
	 * @param roles liste ou on cherche le role le plus haut
	 * */
	public static Role GetHigherRole(ArrayList<Role> roles)
	{
		Role role=null;
		for(Role r : roles)
			if(role==null || r.power>role.power)
				role = r;
		return role;
	}


	/**
	 * Ajoute le role dans la bdd a un joueur
	 * @param player joueur a qui on ajoute le role
	 * @return code de resultat
	 * */
	private CodeRole promotePlayer(OfflinePlayer player)
	{
		if(this.id > 0)
		{
			try {
				PreparedStatement st = Config.GetConfig().getConnection().prepareStatement("insert into "+TABLE_PLAYER_ROLE+" (roleId, player) values(?, ?);");
				st.setInt(1, this.id);
				st.setString(2, player.getUniqueId().toString());
				st.executeUpdate();
				st.close();

				//Role.UpdateTeam(player, Role.GetRolesOf(player.getUniqueId()));

			} catch (SQLException e) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getLocalizedMessage());
				return null;
			}
		}
		else
			return CodeRole.RoleNotFound;
		return CodeRole.Success;
	}


	/**
	 * Supprime le role d'un joueur dans la bdd
	 * @param player joueur a qui on supprime le role
	 * @return code de resultat
	 * */
	private CodeRole demotePlayer(OfflinePlayer player)
	{
		if(this.id>0)
		{
			try {
				PreparedStatement st = Config.GetConfig().getConnection().prepareStatement("delete from "+TABLE_PLAYER_ROLE+" where roleId=? and player=?;");
				st.setInt(1, this.id);
				st.setString(2, player.getUniqueId().toString());
				st.executeUpdate();
				st.close();

				//Role.UpdateTeam(player, Role.GetRolesOf(player.getUniqueId()));

			} catch (SQLException e) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getLocalizedMessage());
				return null;
			}
		}
		else
			return CodeRole.RoleNotFound;
		return CodeRole.Success;
	}


	/**
	 * Get les joueurs qui ont le role
	 * @return liste de joueur qui ont le role
	 * */
	public ArrayList<OfflinePlayer> getPlayers()
	{
		ArrayList<OfflinePlayer> players = new ArrayList<OfflinePlayer>();
		if(this.id>0)
		{
			try {
				Connection con = Config.GetConfig().getConnection();
				PreparedStatement st=con.prepareStatement("Select player from "+TABLE_PLAYER_ROLE+" where roleId=?;");
				st.setInt(1, this.id);
				ResultSet res = st.executeQuery();
				while(res.next())
				{
					players.add(Bukkit.getOfflinePlayer(UUID.fromString(res.getString("player"))));
				}
				res.close();
				st.close();
			} catch (SQLException e) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getLocalizedMessage());
			}
		}
		return players;
	}


	/**
	 * Ajoute une permission a un role dans la bdd
	 * @param role nom du role a qui on ajoute la permission
	 * @param permission permission a ajouter
	 * @return code de resultat
	 * */
	public CodeRole addPermission(String permission)
	{
		if(this.id > 0)
		{
			try {
				Connection con = Config.GetConfig().getConnection();
				PreparedStatement st=con.prepareStatement("Select roleId from "+TABLE_ROLE_NODE+" where roleId=? and node=?;");
				st.setInt(1, this.id);
				st.setString(2, permission);
				ResultSet res = st.executeQuery();
				if(res.next())
				{
					return CodeRole.PermissionAlreadyExist;
				}
			} catch (SQLException e) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getLocalizedMessage());
				return null;
			}
			try {
				PreparedStatement st = Config.GetConfig().getConnection().prepareStatement("insert into "+TABLE_ROLE_NODE+" (roleId, node) values(?, ?);");
				st.setInt(1, this.id);
				st.setString(2, permission);
				st.executeUpdate();

				ArrayList<OfflinePlayer> players = this.getPlayers();
				Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("MyRolePlugin");
				for(OfflinePlayer player: players)
				{
					if(player.isOnline())
					{
						((Player)player).addAttachment(plugin).setPermission(permission, true);
						((Player)player).updateCommands();
					}
				}
			} catch (SQLException e) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getLocalizedMessage());
				return null;
			}
		}
		else
			return CodeRole.RoleNotFound;
		return CodeRole.Success;
	}


	/**
	 * Supprime une permission a un role dans la bdd
	 * @param role nom du role dont on supprime la permission
	 * @param permission permission a supprimer
	 * @return code de resultat
	 * */
	public CodeRole removePermission(String permission)
	{
		if(this.id>0)
		{
			try {
				Connection con = Config.GetConfig().getConnection();
				PreparedStatement st=con.prepareStatement("Select roleId from "+TABLE_ROLE_NODE+" where roleId=? and node=?;");
				st.setInt(1, this.id);
				st.setString(2, permission);
				ResultSet res = st.executeQuery();
				if(!res.next())
				{
					return CodeRole.PermissionNotFound;
				}
			} catch (SQLException e) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getLocalizedMessage());
				return null;
			}
			try {
				PreparedStatement st = Config.GetConfig().getConnection().prepareStatement("delete from "+TABLE_ROLE_NODE+" where roleId=? and node=?;");
				st.setInt(1, this.id);
				st.setString(2, permission);
				st.executeUpdate();

				ArrayList<OfflinePlayer> players = this.getPlayers();
				Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("MyRolePlugin");
				for(OfflinePlayer player: players)
				{
					if(player.isOnline())
					{
						((Player)player).addAttachment(plugin).setPermission(permission, false);
						((Player)player).updateCommands();
					}
				}
			} catch (SQLException e) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getLocalizedMessage());
				return null;
			}
		}
		else
			return CodeRole.RoleNotFound;
		return CodeRole.Success;
	}


	/**
	 * Ajoute un child a un role dans la bdd
	 * @param role nom du role auquel ajouter le child
	 * @param child role fils a ajouter
	 * @return code de resultat
	 * */
	public CodeRole addChildren(Role child)
	{
		if(child!=null && this.id > 0 && child.id>0)
		{
			if(this.power>child.power)
			{
				try {
					Connection con = Config.GetConfig().getConnection();
					PreparedStatement st=con.prepareStatement("Select roleId from "+TABLE_ROLE_CHILD+" where roleId=? and roleChild=?;");
					st.setInt(1, this.id);
					st.setInt(2, child.id);
					ResultSet res = st.executeQuery();
					if(res.next())
					{
						return CodeRole.ChildAlreadyExist;
					}
				} catch (SQLException e) {
					Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getLocalizedMessage());
					return null;
				}
				try {
					PreparedStatement st = Config.GetConfig().getConnection().prepareStatement("insert into "+TABLE_ROLE_CHILD+" (roleId, roleChild) values(?, ?);");
					st.setInt(1, this.id);
					st.setInt(2, child.id);
					st.executeUpdate();

					ArrayList<OfflinePlayer> players = this.getPlayers();
					for(OfflinePlayer player: players)
					{
						child.giveRightsTo(player);
					}
				} catch (SQLException e) {
					Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getLocalizedMessage());
					return null;
				}
			}
			else
				return CodeRole.ChildToHigh;
		}
		else
			return CodeRole.RoleNotFound;
		return CodeRole.Success;
	}


	/**
	 * Supprime un child d'un role dans la bdd
	 * @param role nom du role auquel supprimer le child
	 * @param child role fils a supprimer
	 * @return code de resultat
	 * */
	public CodeRole removeChildren(Role child)
	{
		if(this.id > 0 && child.id>0)
		{
			try {
				Connection con = Config.GetConfig().getConnection();
				PreparedStatement st=con.prepareStatement("Select roleId from "+TABLE_ROLE_CHILD+" where roleId=? and roleChild=?;");
				st.setInt(1, this.id);
				st.setInt(2, child.id);
				ResultSet res = st.executeQuery();
				if(!res.next())
				{
					return CodeRole.ChildNotFound;
				}
			} catch (SQLException e) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
				return null;
			}
			try {
				PreparedStatement st = Config.GetConfig().getConnection().prepareStatement("delete from "+TABLE_ROLE_CHILD+" where roleId=? and roleChild=?;");
				st.setInt(1, this.id);
				st.setInt(2, child.id);
				st.executeUpdate();

				ArrayList<OfflinePlayer> players = this.getPlayers();
				for(OfflinePlayer player: players)
				{
					for(Role iChild : child.getChilds())
						iChild.removeRights(player);
				}
			} catch (SQLException e) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getLocalizedMessage());
				return null;
			}
		}
		else
			return CodeRole.RoleNotFound;
		return CodeRole.Success;
	}


	/**
	 * Get tout les roles
	 * @return liste des noms des roles
	 * */
	public static ArrayList<Role> GetRoles()
	{
		ArrayList<Role> roles=new ArrayList<Role>();

		try {
			Connection con = Config.GetConfig().getConnection();
			PreparedStatement st=con.prepareStatement("Select id, name, power, visible, color from "+TABLE_ROLE+";");
			ResultSet res = st.executeQuery();
			Role role;
			while(res.next())
			{
				role=new Role(
						ChatColor.valueOf(res.getString("color")),
						res.getString("name"),
						res.getInt("power"),
						res.getBoolean("visible")
						);
				role.id=res.getInt("id");
				roles.add(role);
			}
		} catch (SQLException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getLocalizedMessage());
		}

		return roles;
	}


	/**
	 * Get les permissions du role
	 * @return la liste de permissions
	 * */
	public ArrayList<String> getPermissions()
	{
		ArrayList<String> persmissions = new ArrayList<String>();
		if(this.id>0)
		{
			try {
				Connection con = Config.GetConfig().getConnection();
				PreparedStatement st=con.prepareStatement("Select node from "+TABLE_ROLE_NODE+" where roleId=?;");
				st.setInt(1, this.id);
				ResultSet res = st.executeQuery();
				while(res.next())
				{
					persmissions.add(res.getString("node"));
				}
			} catch (SQLException e) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getLocalizedMessage());
			}
		}
		else
			return null;
		return persmissions;
	}

	/**
	 * Get les permissions specifique au joueur
	 * @return la liste de permissions
	 * */
	public static ArrayList<String> GetPlayerPermissions(OfflinePlayer player)
	{
		ArrayList<String> persmissions = new ArrayList<String>();
		try {
			Connection con = Config.GetConfig().getConnection();
			PreparedStatement st=con.prepareStatement("Select node from "+TABLE_PLAYER_PERMISSION+" where player=?;");
			st.setString(1, player.getUniqueId().toString());
			ResultSet res = st.executeQuery();
			while(res.next())
			{
				persmissions.add(res.getString("node"));
			}
		} catch (SQLException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getLocalizedMessage());
		}
		return persmissions;
	}

	public static CodeRole AddPlayerPermission(OfflinePlayer player, String node)
	{
		try {
			PreparedStatement st = Config.GetConfig().getConnection().prepareStatement("insert into "+TABLE_PLAYER_PERMISSION+" (player, node) values(?, ?);");
			st.setString(1, player.getUniqueId().toString());
			st.setString(2, node);
			st.executeUpdate();
		} catch (SQLException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getLocalizedMessage());
			return CodeRole.Failed;
		}
		GivePlayerPermission(player, node);
		return CodeRole.Success;
	}

	private static void GivePlayerPermission(OfflinePlayer player, String node)
	{
		if(player.isOnline())
		{
			Player onlinePlayer = player.getPlayer();
			Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("MyRolePlugin");
			PermissionAttachment attachment = onlinePlayer.addAttachment(plugin);
			attachment.setPermission(node, true);

			onlinePlayer.updateCommands();
			//onlinePlayer.sendMessage(ChatColor.GOLD+"Vous possedez la permission "+node+"!");
		}
	}

	public static CodeRole RemovePlayerPermissions(OfflinePlayer player, String node)
	{
		try {
			PreparedStatement st = Config.GetConfig().getConnection().prepareStatement("delete from "+TABLE_PLAYER_PERMISSION+" where player=? and node=?;");
			st.setString(1, player.getUniqueId().toString());
			st.setString(2, node);
			st.executeUpdate();
		} catch (SQLException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getLocalizedMessage());
			return CodeRole.Failed;
		}
		RemovePlayerPermission(player, node);
		return CodeRole.Success;
	}

	private static void RemovePlayerPermission(OfflinePlayer player, String node)
	{
		if(player.isOnline())
		{
			Player onlinePlayer = player.getPlayer();
			Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("MyRolePlugin");
			PermissionAttachment attachment = onlinePlayer.addAttachment(plugin);
			attachment.setPermission(node, false);

			onlinePlayer.updateCommands();
		}
	}
	
	/**
	 * Get les childs du role
	 * @return la liste de nom des role childs
	 * */
	public ArrayList<Role> getChilds()
	{
		ArrayList<Role> roles=new ArrayList<Role>();

		if(this.id >0)
		{
			try {
				Connection con = Config.GetConfig().getConnection();
				PreparedStatement st=con.prepareStatement("Select id, name, power, visible, color from "+TABLE_ROLE_CHILD+" inner join "+TABLE_ROLE+" on "+TABLE_ROLE+".id="+TABLE_ROLE_CHILD+".roleChild where roleId=?;");
				st.setInt(1, this.id);
				ResultSet res = st.executeQuery();
				Role role;
				while(res.next())
				{
					role=new Role(
							ChatColor.valueOf(res.getString("color")),
							res.getString("name"),
							res.getInt("power"),
							res.getBoolean("visible")
							);
					role.id=res.getInt("id");
					roles.add(role);
				}
			} catch (SQLException e) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getLocalizedMessage());
			}
		}
		return roles;
	}


	/**
	 * Donne un role a un joueur
	 * @param sender personne qui ajoute le role
	 * @param playerName nom du joueur a qui donner le role
	 * @param roleName nom du role a donner
	 * @return code de resultat
	 * */
	public CodeRole promote(CommandSender sender, OfflinePlayer player)
	{
		if(player != null)
		{
			ArrayList<Role> roles = Role.GetRolesOf(player.getUniqueId());
			if(!roles.contains(this))
			{
				if(this.canGiveRoleTo(sender, player))
				{
					this.promotePlayer(player);
					this.giveRightsTo(player);

					if(this.visible)
					{
						roles.add(this);
						Role.UpdateTeam(player, roles);
					}
				}
				else
					return CodeRole.CantGiveThisRole;
			}
			else
				return CodeRole.PlayerHasAlreadyRole;
		}
		else
			return CodeRole.PlayerNotFound;

		return CodeRole.Success;
	}



	/**
	 * Enleve un role a un joueur
	 * @param sender personne qui ajoute le role
	 * @param playerName nom du joueur a qui enlever le role
	 * @param roleName nom du role a donner
	 * @return code de resultat
	 * */
	public CodeRole demote(CommandSender sender, OfflinePlayer player)
	{
		if(player != null)
		{
			ArrayList<Role> roles = Role.GetRolesOf(player.getUniqueId());
			if(roles.contains(this))
			{
				if(this.canGiveRoleTo(sender, player))
				{
					this.demotePlayer(player);
					//remove role rights
					this.removeRights(player);
					//regive rights to be sure
					Role.GiveRightsTo(player);
					if(this.visible)
					{
						roles.remove(this);
						Role.UpdateTeam(player, roles);
					}
				}
				else
					return CodeRole.CantGiveThisRole;
			}
			else
				return CodeRole.PlayerDontHaveRole;
		}
		else
			return CodeRole.PlayerNotFound;

		return CodeRole.Success;
	}


	/**
	 * Enleve tout les roles a un joueur
	 * @param sender personne qui eneleve les roles
	 * @param playerName nom du joueur a qui enlever les roles
	 * @return code de resultat
	 * */
	public static CodeRole Demote(CommandSender sender, String playerName)
	{
		@SuppressWarnings("deprecation")
		OfflinePlayer player = Bukkit.getOfflinePlayer(playerName);
		if(player != null)
		{
			ArrayList<Role> roles = Role.GetRolesOf(player.getUniqueId());
			if(roles.size()>0)
			{
				if(Role.GetHigherRole(roles).canGiveRoleTo(sender, player))
				{
					for(Role r : roles)
					{
						r.demotePlayer(player);
						r.removeRights(player);
					}
					Role.UpdateTeam(player, Role.GetRolesOf(player.getUniqueId()));
				}
				else
					return CodeRole.CantGiveThisRole;
			}
			else
				return CodeRole.PlayerDontHaveRole;
		}
		else
			return CodeRole.PlayerNotFound;

		return CodeRole.Success;
	}


	/**
	 * Compare les roles du sender et d'un joueur
	 * @param sender personne qui lance la commande, not null
	 * @param player joueur a promote, not null
	 * @param role a donner, not null
	 * @return true si le sender a un role superieur au joueur a promote, false si non ou si le joueur est introuvable
	 * */
	private boolean canGiveRoleTo(CommandSender sender, OfflinePlayer player)
	{
		if(sender.isOp())
			return true;
		// verif: rolePlayer < role < roleSender
		if(sender instanceof Player)
		{
			//Bukkit.broadcastMessage("1");
			Player playerSender = (Player) sender;
			ArrayList<Role> rolesSender =Role.GetRolesOf(playerSender.getUniqueId()), 
					rolesPlayer = Role.GetRolesOf(player.getUniqueId());
			Role roleSender = Role.GetHigherRole(rolesSender), 
					rolePlayer = Role.GetHigherRole(rolesPlayer);

			if(roleSender!=null && roleSender.power > this.power && 
					(rolePlayer==null || (rolePlayer.power < roleSender.power)))
				return true;
			else
			{
				//Bukkit.broadcastMessage("2");
				return false;
			}
		}
		else
			return false;
	}

	/**
	 * Give his rights to a player
	 * @param player player to give his rights
	 */
	public static void GiveRightsTo(OfflinePlayer player)
	{
		if(player != null && player.isOnline())
		{
			Player onlinePlayer = (Player) player;

			ArrayList<Role> roles = Role.GetRolesOf(player.getUniqueId());
			for(Role role : roles)
			{
				role.giveRightsTo(player);
			}

			Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("MyRolePlugin");
			PermissionAttachment attachment = onlinePlayer.addAttachment(plugin);
			for(String node : GetPlayerPermissions(player))
				attachment.setPermission(node, true);

			onlinePlayer.updateCommands();
		}
	}

	/**
	 * Give rights to this role to a player
	 * @param player player to give rights to
	 */
	private void giveRightsTo(OfflinePlayer player)
	{
		if(player != null && player.isOnline())
		{
			Player onlinePlayer = (Player) player;
			Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("MyRolePlugin");
			PermissionAttachment attachment = onlinePlayer.addAttachment(plugin);

			for(String node : this.getPermissions())
				attachment.setPermission(node, true);
			for(Role child : this.getChilds())
				child.giveRightsTo(onlinePlayer);
			onlinePlayer.updateCommands();
		}
	}

	/**
	 * Remove rights to a player (after a demote)
	 * @param player player to remove rights
	 */
	private void removeRights(OfflinePlayer player)
	{
		if(player != null && player.isOnline())
		{
			Player onlinePlayer = (Player) player;
			Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("MyRolePlugin");
			PermissionAttachment attachment = onlinePlayer.addAttachment(plugin);

			for(String node : this.getPermissions())
				attachment.setPermission(node, false);
			for(Role child : this.getChilds())
				child.removeRights(onlinePlayer);
			onlinePlayer.updateCommands();
		}
	}

	/**
	 * compare les id des roles
	 * @return true si les id sont identique
	 * */
	public boolean equals(Object object)
	{
		return object != null && object instanceof Role && this.id==((Role)object).id && this.id!=-1;
	}

	/**
	 * String representation of object Role
	 */
	public String toString()
	{
		return this.color+this.name+ChatColor.WHITE+", power="+this.power+", visible="+this.visible;
	}
}