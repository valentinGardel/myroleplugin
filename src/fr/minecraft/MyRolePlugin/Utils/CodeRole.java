package fr.minecraft.MyRolePlugin.Utils;

public enum CodeRole {
	Success,
	RoleAlreadyExist,
	RoleNotFound, 
	PermissionAlreadyExist, 
	PermissionNotFound, 
	PlayerHasAlreadyRole,
	PlayerNotFound,
	CantGiveThisRole,
	ChildToHigh, 
	ChildInvalid, 
	PlayerDontHaveRole,
	ChildAlreadyExist, 
	ChildNotFound, 
	Failed
}